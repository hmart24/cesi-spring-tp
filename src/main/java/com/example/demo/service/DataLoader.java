package com.example.demo.service;

import com.example.demo.domain.Product;
import com.example.demo.domain.Seller;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class DataLoader {
    SellerRepository sellerRepository;
    ProductRepository productRepository;

    @Autowired
    public DataLoader(SellerRepository sellerRepository, ProductRepository productRepository) {
        this.sellerRepository = sellerRepository;
        this.productRepository = productRepository;
    }
    /*
 [
  {
    "description": "in quod dolores voluptatum quia\narchitecto ut officiis consequatur velit sed et\ntotam nam ipsum aut voluptas voluptates",
    "Listing Name": [
      "suscipit",
      "architecto",
      "cupiditate"
    ],
    "imageUrl": "adolf.name",
    "Price": 144
  },
  {
    "description": "quasi minima enim rerum ullam accusamus laborum voluptate\nhic corporis sint voluptas\nautem quas quae non nam et voluptas",
    "Listing Name": [
      "cum",
      "quidem",
      "et"
    ],
    "imageUrl": "chandler.io",
    "Price": 200
  },
  {
    "description": "sed nisi fugit officia nostrum sit saepe ut\ndolores modi animi\nitaque illo est commodi rerum praesentium aut et",
    "Listing Name": [
      "dolore",
      "voluptatum",
      "impedit"
    ],
    "imageUrl": "tiffany.info",
    "Price": 994
  },
  {
    "description": "quas sapiente mollitia dolores eligendi expedita assumenda voluptates\ndebitis aut alias corporis non occaecati\na iure magni at omnis asperiores",
    "Listing Name": [
      "ut",
      "quod",
      "sit"
    ],
    "imageUrl": "teagan.tv",
    "Price": 265
  },
  {
    "description": "sit ea in rem incidunt enim inventore sed\nillo rerum fugiat maxime qui laudantium\nlaboriosam et eligendi aperiam eaque enim placeat odit quia",
    "Listing Name": [
      "libero",
      "temporibus",
      "illo"
    ],
    "imageUrl": "walton.name",
    "Price": 783
  },
  {
    "description": "harum suscipit perspiciatis delectus inventore veniam non\ndebitis voluptas totam doloribus porro sit reiciendis\nfacilis quis incidunt placeat dolorum quo fuga aut laboriosam",
    "Listing Name": [
      "eligendi",
      "eum",
      "consectetur"
    ],
    "imageUrl": "muriel.tv",
    "Price": 351
  },
  {
    "description": "aperiam facere vel voluptate\nvoluptatem natus ut ipsa autem\nvoluptatum soluta qui rerum iure enim doloribus ipsum",
    "Listing Name": [
      "in",
      "quia",
      "quod"
    ],
    "imageUrl": "hans.biz",
    "Price": 671
  },
  {
    "description": "accusamus dolor magni eligendi nemo et sed\nexplicabo quis accusamus est\nest ad officia in recusandae nihil laudantium nobis",
    "Listing Name": [
      "sapiente",
      "voluptatibus",
      "possimus"
    ],
    "imageUrl": "clark.biz",
    "Price": 748
  },
  {
    "description": "autem minima et consequatur sit magnam soluta quo aut\nminima ea quia omnis nam aliquam dolore temporibus voluptas\nvel in est velit voluptatum modi rem quia molestiae",
    "Listing Name": [
      "placeat",
      "enim",
      "nostrum"
    ],
    "imageUrl": "green.us",
    "Price": 800
  },
  {
    "description": "laborum facere sapiente expedita dolores sed\nqui quidem voluptatem fugit cumque\nrecusandae dolor perspiciatis itaque id",
    "Listing Name": [
      "necessitatibus",
      "doloribus",
      "molestiae"
    ],
    "imageUrl": "alexzander.tv",
    "Price": 188
  }
]
     */
    @PostConstruct
    private void loadData() {
        Seller seller1 = new Seller("Hugo Martinez", "50b rue des roseaux", 31400, "Toulouse");
        Seller seller2 = new Seller("Savion Bashirian", "3258 Runte Forest", 8467, "Lake Garret");
        Seller seller3 = new Seller("Neil Nader", "082 Gorczany Passage", 92936, "Beahanshire");
        Product product1 = new Product("suscipit", "in quod dolores voluptatum quia architecto ut officiis consequatur velit sed et totam nam ipsum aut voluptas voluptates", "adolf.name", 144, seller1);
        Product product2 = new Product("cum", "quasi minima enim rerum ullam accusamus laborum voluptate hic corporis sint voluptas autem quas quae non nam et voluptas", "chandler.io", 200, seller2);
        Product product3 = new Product("dolore", "sed nisi fugit officia nostrum sit saepe ut dolores modi animi itaque illo est commodi rerum praesentium aut et", "tiffany.info", 994, seller3);
        Product product4 = new Product("ut", "quas sapiente mollitia dolores eligendi expedita assumenda voluptates debitis aut alias corporis non occaecati a iure magni at omnis asperiores", "teagan.tv", 265, seller1);
        Product product5 = new Product("libero", "sit ea in rem incidunt enim inventore sed illo rerum fugiat maxime qui laudantium laboriosam et eligendi aperiam eaque enim placeat odit quia", "walton.name", 783, seller2);
        Product product6 = new Product("eligendi", "harum suscipit perspiciatis delectus inventore veniam non debitis voluptas totam doloribus porro sit reiciendis facilis quis incidunt placeat dolorum quo fuga aut laboriosam", "muriel.tv", 351, seller3);
        Product product7 = new Product("in", "aperiam facere vel voluptate voluptatem natus ut ipsa autem voluptatum soluta qui rerum iure enim doloribus ipsum", "hans.biz", 671, seller1);
        Product product8 = new Product("sapiente", "accusamus dolor magni eligendi nemo et sed explicabo quis accusamus est est ad officia in recusandae nihil laudantium nobis", "clark.biz", 748, seller2);
        Product product9 = new Product("placeat", "autem minima et consequatur sit magnam soluta quo aut minima ea quia omnis nam aliquam dolore temporibus voluptas vel in est velit voluptatum modi rem quia molestiae", "green.us", 800, seller3);
        sellerRepository.save(seller1);
        sellerRepository.save(seller2);
        sellerRepository.save(seller3);
        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);
        productRepository.save(product5);
        productRepository.save(product6);
        productRepository.save(product7);
        productRepository.save(product8);
        productRepository.save(product9);
    }
}
