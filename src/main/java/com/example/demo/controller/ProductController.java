package com.example.demo.controller;

import com.example.demo.domain.Product;
import com.example.demo.domain.Seller;
import com.example.demo.exceptions.SellerNotFoundException;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController {
    private ProductRepository productRepository;
    private SellerRepository sellerRepository;

    @Autowired
    public ProductController(SellerRepository sellerRepository, ProductRepository productRepository) {
        this.sellerRepository = sellerRepository;
        this.productRepository = productRepository;
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    private class ProductNotFoundException extends RuntimeException {
        ProductNotFoundException() {
            super("Product not found.");
        }
    }

    @GetMapping(path="/api/products", produces="application/json")
    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @PostMapping(path="/api/sellers/{sellerId}/products", produces="application/json", consumes="application/json")
    public Product createProduct(@RequestBody Product newProduct, @PathVariable long sellerId) {
        Optional<Seller> optionalSeller = sellerRepository.findById(sellerId);
        if(!optionalSeller.isPresent()) {
            throw new SellerNotFoundException();
        }
        newProduct.setSeller(optionalSeller.get());
        productRepository.save(newProduct);
        return newProduct;
    }

    @GetMapping(path="/api/sellers/{sellerId}/products", produces="application/json", consumes="application/json")
    public List<Product> getProductsBySeller(@PathVariable long sellerId) {
        Optional<Seller> optionalSeller = sellerRepository.findById(sellerId);
        if(!optionalSeller.isPresent()) {
            throw new SellerNotFoundException();
        }
        return productRepository.findBySeller(optionalSeller.get());
    }
    @GetMapping(path = "/api/products/{id}", produces="application/json")
    public Product getProductById(@PathVariable Long id) {
        Optional<Product> optional = productRepository.findById(id);
        if(!optional.isPresent()) {
            throw new ProductNotFoundException();
        }
        return optional.get();
    }
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    @DeleteMapping(path="/api/products/{id}", produces="application/json")
    public void deleteProduct(@PathVariable Long id) {
        try {
            productRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException exc) {

        }
    }
}
