package com.example.demo.controller;

import com.example.demo.exceptions.SellerHasProductsException;
import com.example.demo.exceptions.SellerNotFoundException;
import com.example.demo.domain.Seller;
import com.example.demo.repository.SellerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class SellerController {
    private SellerRepository sellerRepository;

    @Autowired
    public SellerController(SellerRepository sellerRepository) {
        this.sellerRepository = sellerRepository;
    }

    @GetMapping(path = "/api/sellers", produces="application/json")
    public Iterable<Seller> index() {
        return sellerRepository.findAll();
    }
    @PostMapping(path = "/api/sellers", produces="application/json", consumes = "application/json")
    public Seller create(@RequestBody Seller newSeller) {
        sellerRepository.save(newSeller);
        return newSeller;
    }
    @GetMapping(path = "/api/sellers/{id}", produces="application/json")
    public Seller getById(@PathVariable Long id) {
        Optional<Seller> optional = sellerRepository.findById(id);
        if(!optional.isPresent()) {
            throw new SellerNotFoundException();
        }
        return optional.get();
    }
    @ResponseStatus(value= HttpStatus.NO_CONTENT)
    @DeleteMapping(path="/api/sellers/{id}", produces="application/json")
    public void deleteProduct(@PathVariable Long id) {
        Optional<Seller> optional = sellerRepository.findById(id);
        if(!optional.isPresent()) {
            throw new SellerNotFoundException();
        }
        if(!optional.get().getProducts().isEmpty()) {
            throw new SellerHasProductsException();
        }
        sellerRepository.deleteById(id);
    }
}
