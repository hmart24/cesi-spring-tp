package com.example.demo.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@Entity
public class Seller {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String name;
    private String address;
    @Max(99999)
    private int zipCode;
    private String city;
    @OneToMany(mappedBy="seller")
    private Collection<Product> products;

    public Seller() {
        this.products = new ArrayList<>();
    }

    public Seller(String name, String address, @Max(99999) int zipCode, String city) {
        this.products = new ArrayList<>();
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
    }

    public Collection<Product> getProducts() {
        return products;
    }
}
