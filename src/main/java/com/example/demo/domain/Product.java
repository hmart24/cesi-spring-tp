package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String imageUrl;
    @Column(precision=16, scale=2)
    private double price;
    private LocalDateTime createdOn;
    @OneToOne
    @JsonBackReference
    private Seller seller;

    public Product(String name, String description, String imageUrl, double price, Seller seller) {
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.price = price;
        this.seller = seller;
        this.createdOn = LocalDateTime.now();
    }

    public Product() {
        createdOn = LocalDateTime.now();
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }
}


