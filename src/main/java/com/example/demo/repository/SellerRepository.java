package com.example.demo.repository;

import com.example.demo.domain.Seller;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SellerRepository extends CrudRepository<Seller, Long> {
    @Query(value="SELECT s.*, SUM(price) sumPrice FROM Seller s INNER JOIN Product p ON p.seller_id = s.id GROUP BY p.seller_id ORDER BY sumPrice DESC LIMIT 1", nativeQuery=true)
    Seller findBestSellerValue();
}
