package com.example.demo.repository;

import com.example.demo.domain.Product;
import com.example.demo.domain.Seller;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findByPriceIsGreaterThanOrderByPriceAsc(double price);
    List<Product> findByPriceIsLessThanOrderByPriceAsc(double price);
    List<Product> findBySeller(Seller seller);
}
