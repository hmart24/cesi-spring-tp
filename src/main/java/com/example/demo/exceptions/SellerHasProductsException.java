package com.example.demo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST)
public class SellerHasProductsException extends RuntimeException {
    public SellerHasProductsException() {
        super("Cannot delete seller because it still has products.");
    }
}
